# public_examples



## Prerequisites

- ROS Melodic or Noetic (see [Kinetic Installation Guide](http://wiki.ros.org/kinetic/Installation/Ubuntu), [Melodic Installation Guide](http://wiki.ros.org/melodic/Installation/Ubuntu), [Noetic Installation Guide](http://wiki.ros.org/noetic/Installation/Ubuntu))

- Install ROS packages (see [ROS Wiki](http://wiki.ros.org/ros_control)):
    For ROS Melodic

  ```shell
  sudo apt install ros-melodic-controller-manager ros-melodic-gazebo-ros-control ros-melodic-ros-controllers ros-melodic-ros-control ros-melodic-moveit ros-melodic-rviz-imu-plugin ros-melodic-global-planner ros-melodic-dwa-local-planner ros-melodic-move-base ros-melodic-hector-mapping
  ```
    For ROS Noetic

  ```shell
  sudo apt install ros-noetic-controller-manager ros-noetic-gazebo-ros-control ros-noetic-ros-controllers ros-noetic-ros-control ros-noetic-moveit ros-noetic-rviz-imu-plugin ros-noetic-global-planner ros-noetic-dwa-local-planner ros-noetic-move-base ros-noetic-hector-mapping
  ```

  ## Build and Run

  - Clone the project recursively

    ```
    git clone https://gitlab.com/LIRS_Projects/public_examples.git
    ```

  - Build the project (catkin build):

      ```shell
      cd ~/catkin_ws

      catkin_make

      source devel/setup.bash
      ```
