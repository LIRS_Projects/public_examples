<robot xmlns:xacro="http://www.ros.org/wiki/xacro">

  <!-- arm properties -->
<!--  joints velocity limit-->
  <xacro:property name="joints_vlimit" value="${radians(180) / 4}"/>

<!--waist and shoulder links masses-->
  <xacro:property name="waist_mass" value="0.477"/>
  <xacro:property name="shoulder_mass" value="2.231"/>

<!--  upper and lower limits of the shoulder joint-->
  <xacro:property name="shoulder_llimit" value="${0}"/>
  <xacro:property name="shoulder_ulimit" value="${radians(114.2)}"/>

<!--  friction parameters-->
  <xacro:property name="friction" value="1.0"/>
  <xacro:property name="damping" value="1.0"/>

<!--  PID controler parameters for the joint-->
  <xacro:property name="kp" value="1e6"/>
  <xacro:property name="kd" value="1"/>

<!--  macros for calculating cudoid inertia based on length, width an height of the block, and its mass, and origin-->
  <xacro:macro name="cuboid_inertia" params="length width height mass *origin">
    <inertial>
      <mass value="${mass}"/>
      <xacro:insert_block name="origin"/>
      <inertia ixx="${mass/12*(height**2 + length**2)}" ixy="0.0" ixz="0.0"
               iyy="${mass/12*(width**2 + length**2)}" iyz="0.0"
               izz="${mass/12*(width**2 + height**2)}"/>
    </inertial>
  </xacro:macro>

    <!--  waist  -->
    <link name="waist_link">
      <visual>
        <geometry>
          <mesh filename="package://engineer_description/meshes/scaled_cronstain.dae" scale="0.145 0.145 0.145"/> -->
        </geometry>
      </visual>
      <collision>
        <geometry>
          <mesh filename="package://engineer_description/meshes/collision/scaled_Cronstain.dae" scale="0.145 0.145 0.145"/>
        </geometry>
      </collision>
      <xacro:cuboid_inertia mass="${waist_mass}" length="0.08" width="0.08" height="0.08">
        <origin xyz="0 0 0" rpy="0 0 0"/>
      </xacro:cuboid_inertia>
    </link>


    <gazebo reference="waist_link">
      <selfCollide>false</selfCollide>
      <kp>${kp}</kp>
      <kd>${kd}</kd>
      <mu1>100</mu1>
      <mu2>50</mu2>
    </gazebo>


    <!--  shoulder  -->
    <link name="shoulder_link">
      <visual>
        <geometry>
          <mesh filename="package://example_description/meshes/scaled_shoulder.dae" scale="0.1 0.1 0.1"/>
        </geometry>
      </visual>
      <collision>
        <geometry>
          <mesh filename="package://example_description/meshes/collision/scaled_Shoulder.dae" scale="0.1 0.1 0.1"/>
        </geometry>
      </collision>
      <inertial>
        <mass value="${shoulder_mass}"/>
        <origin xyz="-0.0303 -0.0001 0.1511" rpy="0 0 0"/>

        <inertia ixx="0.0295383" ixy="-0.0000001" ixz="-0.0004068"
                 iyy="0.0292352" iyz="0.0000004"
                 izz="0.0011211"/>
      </inertial>
    </link>

    <joint name="shoulder" type="revolute">
      <parent link="waist_link"/>
      <child link="shoulder_link"/>
      <axis xyz="1 0 0"/>
      <dynamics friction="${friction}" damping="${damping}"/>
      <origin xyz="0.036 0.051 -0.07" rpy="${pi} 0 0"/>
      <limit lower="${shoulder_llimit}" upper="${shoulder_ulimit}" effort="${shoulder_mass * 50}"
             velocity="${joints_vlimit}"/>
    </joint>

    <gazebo reference="shoulder_link">
      <selfCollide>false</selfCollide>
      <kp>${kp}</kp>
      <kd>${kd}</kd>
      <mu1>100</mu1>
      <mu2>50</mu2>
    </gazebo>

    <transmission name="waist_shoulder_trans">
      <type>transmission_interface/SimpleTransmission</type>
      <actuator name="waist_shoulder_motor">
        <mechanicalReduction>1</mechanicalReduction>
      </actuator>
      <joint name="shoulder">
        <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
      </joint>
    </transmission>



    <xacro:macro name="head_camera" params ="number width height fps *origin">
      <joint name="camera${number}_joint" type="fixed">
        <axis xyz="0 1 0" />
        <xacro:insert_block name="origin"/>
        <parent link="shoulder_link"/>
        <child link="camera${number}_link"/>
      </joint>

      <!-- Camera -->
      <link name="camera${number}_link">
        <collision>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
            <box size="0.0001 0.0001 0.0001"/>
          </geometry>
        </collision>

        <visual>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
            <box size="0.0001 0.0001 0.0001"/>
          </geometry>
        </visual>

        <xacro:cuboid_inertia mass="0.1" length="0.01" width="0.01" height="0.01">
          <origin xyz="0 0 0" rpy="0 0 0"/>
        </xacro:cuboid_inertia>
      </link>


      <joint name="camera${number}_optical_joint" type="fixed">
        <origin xyz="0 0 0" rpy="${-pi/2} 0 ${-pi/2}"/>
        <parent link="camera${number}_link"/>
        <child link="camera${number}_link_optical"/>
      </joint>

      <link name="camera${number}_link_optical">
      </link>

      <gazebo reference="camera${number}_link">
        <sensor type="camera" name="camera_${number}">
          <update_rate>${fps}</update_rate>
          <camera name="head_${number}">
            <horizontal_fov>1.3962634</horizontal_fov>
            <image>
              <width>${width}</width>
              <height>${height}</height>
              <format>R8G8B8</format>
            </image>
            <clip>
              <near>0.02</near>
              <far>300</far>
            </clip>
            <noise>
              <type>gaussian</type>
              <mean>0.0</mean>
              <stddev>0.007</stddev>
            </noise>
          </camera>
          <plugin name="camera${number}_controller" filename="libgazebo_ros_camera.so">
            <alwaysOn>true</alwaysOn>
            <updateRate>0.0</updateRate>
            <cameraName>camera${number}</cameraName>
            <imageTopicName>image${number}_raw</imageTopicName>
            <cameraInfoTopicName>camera_info</cameraInfoTopicName>
            <frameName>camera${number}_link_optical</frameName>]
            <hackBaseline>0.0</hackBaseline>
            <distortionK1>0.0</distortionK1>
            <distortionK2>0.0</distortionK2>
            <distortionK3>0.0</distortionK3>
            <distortionT1>0.0</distortionT1>
            <distortionT2>0.0</distortionT2>
            <CxPrime>0</CxPrime>
            <Cx>0.0</Cx>
            <Cy>0.0</Cy>
            <focalLength>0.0</focalLength>
          </plugin>
        </sensor>
      </gazebo>
    </xacro:macro>

    <xacro:head_camera number="1" width="1280" height="720" fps="50.0">
      <origin xyz="-0.035 0.11 -0.04" rpy="-${pi/2} ${pi/2} 0"/>
    </xacro:head_camera>

    <xacro:head_camera number="2" width="640" height="480" fps="30.0">
      <origin xyz="0.13 0.11 -0.04" rpy="-${pi/2} ${pi/2} 0"/>
    </xacro:head_camera>


</robot>
